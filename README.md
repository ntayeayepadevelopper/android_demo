# Android Demo

A reference Android project using [GitLab Mobile DevOps](https://about.gitlab.com/handbook/engineering/incubation/mobile-devops/) and [Fastlane](http://fastlane.tools/).

## Getting Started with GitLab Mobile DevOps for Android

There are few steps required in order to automate a build, sign, and release pipeline for an Android application. The instructions below will walk you through each step. GitLab Mobile DevOps uses [Fastlane](http://fastlane.tools/). See the [installation instructions](https://docs.fastlane.tools/getting-started/android/setup/) for details on how to install it via Homebrew or RubyGems. 

### Initialize Fastlane

Ensure the application has a `Gemfile` in the root directory with the following contents:

```ruby
source "https://rubygems.org"

gem 'fastlane'
```

Then run `bundle install` to install Fastlane and all of it's dependencies.

Once installed, setting up Fastlane is often as easy as running the following command.

```
bundle exec fastlane init
```

This command will create a `fastlane` folder in the project with an `Appfile` and a stubbed out `Fastfile`. During this process it will ask you for your app's package name and secret file (skip the secret file). When finished, you will end up with an `Appfile` that looks similar to [this](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/android_demo/-/blob/main/fastlane/Appfile).

### Code Signing

#### Create a Keystore

The next step is to create keystore and properties files for code signing. Run the following command to generate a keystore file:

```
keytool -genkey -v -keystore release-keystore.jks -storepass password -alias release -keypass password -keyalg RSA -keysize 2048 -validity 10000
```

For more information on this command, see the [keytool docs](https://download.java.net/java/early_access/loom/docs/specs/man/keytool.html).

Running this command will create a file in the project root called `release-keystore.jks`.

The next step is to create a properties file to be used by the Gradle. Create a file in the project root with the following contents:

```
storeFile=release-keystore.jks
keyAlias=release
keyPassword=password
storePassword=password
```

With these files created, now [upload them to Secure Files](https://docs.gitlab.com/ee/ci/secure_files) in the GitLab project so they can be used in the CI jobs.

Also, be sure to add both of these files to your `.gitignore` file so they aren't commited to version control.


#### Configure Gradle

The next step is to configure Gradle to use the newly created keystore. In the `app/build.gradle` file add the following:

1. Right after the plugins section, add:

	```
	def keystoreProperties = new Properties()
	def keystorePropertiesFile = rootProject.file('release-keystore.properties')
	if (keystorePropertiesFile.exists()) {
		keystoreProperties.load(new FileInputStream(keystorePropertiesFile))
	}
	```
1. Before Build Types, add: 

	```
	signingConfigs {
    	release {
        	keyAlias keystoreProperties['keyAlias']
        	keyPassword keystoreProperties['keyPassword']
        	storeFile keystoreProperties['storeFile'] ? file(keystoreProperties['storeFile']) : null
        	storePassword keystoreProperties['storePassword']
    	}
	}
	```
1. Lastly, add the signingConfig to the release build type:

	```
	signingConfig signingConfigs.release
	```

### Create a CI Pipeline

With the configuration in place, now add the `.gitlab-ci.yml` and `fastlane/Fastfile` to the project.

* This [.gitlab-ci.yml](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/android_demo/-/blob/main/.gitlab-ci.yml) has all the configuration needed to run the `test`, `build` and `beta` jobs.
* The [fastlane/Fastfile](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/android_demo/-/blob/main/fastlane/Fastfile) is an example that can be customized to the specific project settings.

Note: This Fastlane configuration uses plugins. See the [docs](https://docs.fastlane.tools/plugins/using-plugins/) for instructions on how to configure your project for Fastlane plugins.

### Create an App in the Google Play Console

Next, generate a build of your app locally and upload it to seed a new app entry in the Google Play console. Run the following command locally:

```
bundle exec fastlane build
```

This command will create a signed build of the app at: 

```
build/outputs/bundle/release/app-release.aab
```

With the signed build ready to go, login to the [Google Play Console](https://play.google.com/console) and create a new app and seed it with the initial build.

### Configure Google Play Integration

The last thing to set up is the Google Play integration in GitLab. To do so, first create a Google service account.

#### Create a Google Service Account

Follow the [instructions](https://docs.fastlane.tools/actions/supply/#setup) for setting up a service account in Google Cloud Platform and granting that account access to the project in Google Play.

#### Enable Google Play Integration

Follow the [instructions](https://docs.gitlab.com/ee/user/project/integrations/google_play.html) for configuring the Google Play integration by providing a package name and the JSON key file just generated for the service account.
